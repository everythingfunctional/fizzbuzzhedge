module FizzbuzzTest
    ( test
    )
where

import           Fizzbuzz                       ( fizzbuzz )
import           Hedge                          ( Test
                                                , Result
                                                , and
                                                , assertAll
                                                , assertEquals
                                                , assertIncludes
                                                , delimit
                                                , describe
                                                , emptyResult
                                                , fail'
                                                , it
                                                , succeed
                                                )
import           Prelude                 hiding ( and )
import           Text.Read                      ( readMaybe )

fizzbuzzes = take 100 fizzbuzz

test :: IO (Test ())
test = return $ describe
    "fizzbuzz"
    [ it "Each item is Fizz, Buzz, FizzBuzz or a number"
         checkPossibleWordOrNumber
    , it "If item is a number, it is the position in the list"
         checkIfNumberIsPosition
    , it "Every third item has Fizz" $ checkEveryNHas 3 "Fizz"
    , it "Every fifth item has Buzz" $ checkEveryNHas 5 "Buzz"
    , it "Every fifteenth item has FizzBuzz" $ checkEveryNHas 15 "FizzBuzz"
    , it "If item is a number, its position is not divisible by 3"
        $ checkIfIsNumberPositionNotDivisbleBy 3
    , it "If item is a number, its position is not divisible by 5"
        $ checkIfIsNumberPositionNotDivisbleBy 5
    , it "If item is Fizz, its position is divisble by 3"
        $ checkIfIsPositionDivisibleBy "Fizz" 3
    , it "If item is Buzz, its position is divisible by 5"
        $ checkIfIsPositionDivisibleBy "Buzz" 5
    , it "If item is FizzBuzz, its position is divisible by 15"
        $ checkIfIsPositionDivisibleBy "FizzBuzz" 15
    ]

checkPossibleWordOrNumber :: Result
checkPossibleWordOrNumber = assertAll possibleWordOrNumber fizzbuzzes

possibleWordOrNumber :: String -> Result
possibleWordOrNumber str =
    let words' = ["Fizz", "Buzz", "FizzBuzz"]
    in  if (str `elem` words') || isNumber str
            then
                succeed
                    (  delimit str
                    ++ " was one of "
                    ++ show words'
                    ++ " or a number"
                    )
            else fail'
                (  "Expected "
                ++ delimit str
                ++ " to be one of "
                ++ show words'
                ++ " or a number"
                )

isNumber :: String -> Bool
isNumber str = case readMaybe str :: Maybe Int of
    Just _  -> True
    Nothing -> False

checkIfNumberIsPosition :: Result
checkIfNumberIsPosition = assertAll ifNumberIsPosition $ zip fizzbuzzes [1 ..]

ifNumberIsPosition :: (String, Int) -> Result
ifNumberIsPosition (str, pos) = if not (isNumber str)
    then succeed (delimit str ++ " was not a number")
    else assertEquals (show pos) str

checkEveryNHas :: Int -> String -> Result
checkEveryNHas n search =
    assertAll (assertIncludes search) $ every n fizzbuzzes

every n xs = case drop (n - 1) xs of
    (y : ys) -> y : every n ys
    []       -> []

checkIfIsNumberPositionNotDivisbleBy :: Int -> Result
checkIfIsNumberPositionNotDivisbleBy n =
    assertAll (ifNumberIsNotDivisibleBy n) $ zip fizzbuzzes [1 ..]

ifNumberIsNotDivisibleBy :: Int -> (String, Int) -> Result
ifNumberIsNotDivisibleBy n (str, pos) = if not (isNumber str)
    then succeed (delimit str ++ " was not a number")
    else assertNotDivisibleBy n pos

assertNotDivisibleBy :: Int -> Int -> Result
assertNotDivisibleBy divisor numerator = if numerator `mod` divisor /= 0
    then succeed
        (delimit (show numerator) ++ " was not divisble by " ++ delimit
            (show divisor)
        )
    else fail'
        (delimit (show numerator) ++ " was divisble by " ++ delimit
            (show divisor)
        )

checkIfIsPositionDivisibleBy :: String -> Int -> Result
checkIfIsPositionDivisibleBy str n =
    assertAll (ifIsDivisbleBy str n) $ zip fizzbuzzes [1 ..]

ifIsDivisbleBy :: String -> Int -> (String, Int) -> Result
ifIsDivisbleBy search n (str, pos) = if search == str
    then assertDivisibleBy n pos
    else succeed (delimit str ++ " was not " ++ delimit search)

assertDivisibleBy :: Int -> Int -> Result
assertDivisibleBy divisor numerator = if numerator `mod` divisor == 0
    then succeed
        (delimit (show numerator) ++ " was divisble by " ++ delimit
            (show divisor)
        )
    else fail'
        (delimit (show numerator) ++ " was not divisble by " ++ delimit
            (show divisor)
        )
